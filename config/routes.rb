Rails.application.routes.draw do
 
  devise_for :users , :skip => [:registrations]
  as :user do
      get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'    
      put 'users/:id' => 'devise/registrations#update', :as => 'user_registration'            
  end

  resources :posts do
    resources :comments
  end
 
  root "posts#index"

  get '/about', to: 'pages#about'
  # this way, you don't have to make it blog/pages/about... it only has to be blog/about

end